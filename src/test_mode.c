/* C Library Includes */

#include <stdbool.h>
#include <stdint.h>

/* Application Includes */

#include "app_display.h"
#include "app_test_mode.h"

/* Private Variables */

static uint16_t s_count = 0;
static uint8_t s_timer = 0;

/* Public Functions */

void test_mode_tick(DisplayData * p_display_data)
{
    s_timer += 10;
    if (s_timer >= 250)
    {
        s_timer = 0;
        s_count++;
        if (s_count == 999) { s_count = 0;}
        p_display_data->digits[0] = s_count / 100;
        p_display_data->digits[1] = (s_count % 100) / 10;
        p_display_data->digits[2] = s_count % 10;

        if (p_display_data->dp[0])
        {
            p_display_data->dp[0] = p_display_data->dp[1] = p_display_data->dp[2] = false;
        }
        else
        {
            p_display_data->dp[0] = p_display_data->dp[1] = p_display_data->dp[2] = true;
        }
    }
}
