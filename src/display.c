/* C Library Includes */
#include <stdbool.h>

/* AVR Includes */

#include <avr/io.h>
#include <avr/interrupt.h>

/* Library Includes */

#include "seven_segment_map.h"

/* Application Includes */

#include "app_pins.h"
#include "app_display.h"

/* Private Variables */

static volatile uint8_t s_current_digit = 0;
static volatile DisplayData * s_p_data;
static volatile uint8_t s_segment_data_map[16];

static uint16_t s_off_interval_ms = 0;
static uint16_t s_on_interval_ms = 0;
static uint16_t s_blink_timer = 0;
static volatile bool s_blink_state = true;
static volatile bool s_enable = true;

static const SEVEN_SEGMENT_MAP s_map =
{
    SEGMENT_A_PIN, // A
    SEGMENT_B_PIN, // B
    SEGMENT_C_PIN, // C
    SEGMENT_D_PIN, // D
    SEGMENT_E_PIN, // E
    SEGMENT_F_PIN, // F
    SEGMENT_G_PIN, // G
    SEGMENT_DP_PIN, // DP
};

/* Public Functions */

void display_init(DisplayData * pDisplay)
{
    s_p_data = pDisplay;

    uint8_t i;
    for (i = 0; i < 15; ++i)
    {
        s_segment_data_map[i] = SSEG_CreateDigit(i, &s_map, true);
    }

    // Timer clock = 1MHz / 8 = 125kHz
    // Overflow frequency: 488Hz
    TCCR0 = (1<<CS01);
    TIMSK |= (1 << TOIE0);
}

void display_blink_set_on_interval(uint16_t interval_ms)
{
    s_on_interval_ms = (((interval_ms + 5) / 10) * 10);
    s_blink_timer = s_on_interval_ms;
    s_blink_state = true;
}

void display_blink_set_off_interval(uint16_t interval_ms)
{
    s_off_interval_ms = (((interval_ms + 5) / 10) * 10);
}

void display_enable(bool en)
{
    s_enable = en;
}

void display_blink_tick(void)
{
    if (s_on_interval_ms && s_off_interval_ms)
    {
        if (s_blink_timer)
        {
            s_blink_timer -= 10;
        }

        if (s_blink_timer == 0)
        {
            s_blink_state = !s_blink_state;
            s_blink_timer = s_blink_state ? s_on_interval_ms : s_off_interval_ms;
        }
    }
    else
    {
        s_blink_state = true;
    }
}

ISR(TIMER0_OVF0_vect)
{
    // Turn off current digit
    switch(s_current_digit)
    {
    case 0:
        DIGIT_PORT &= ~(DIGIT1_PINMASK);
        break;
    case 1:
        DIGIT_PORT &= ~(DIGIT2_PINMASK);
        break;
    case 2:
        DIGIT_PORT &= ~(DIGIT3_PINMASK);
        break;
    }

    // Select next digit
    s_current_digit++;
    if (s_current_digit == 3) { s_current_digit = 0;}

    // Setup data
    uint8_t data = s_segment_data_map[s_p_data->digits[s_current_digit]];
    SSEG_AddDecimal(&data, &s_map, s_p_data->dp[s_current_digit]);

    SEGMENT_PORT = data;

    // Turn on new digit
    if (s_enable && s_blink_state)
    {
        switch(s_current_digit)
        {
        case 0:
            DIGIT_PORT |= DIGIT1_PINMASK;
            break;
        case 1:
            DIGIT_PORT |= DIGIT2_PINMASK;
            break;
        case 2:
            DIGIT_PORT |= DIGIT3_PINMASK;
            break;
        }
    }
}
