/* C Library Includes */
#include <stdbool.h>
#include <string.h>

/* AVR Includes */

#include <avr/io.h>
#include <util/atomic.h>
#include <avr/interrupt.h>

/* Code Library Includes */

#include "check-and-clear.h"
#include "usi_i2c_slave.h"

/* Application Includes */

#include "app_pins.h"
#include "app_display.h"
#include "app_registers.h"
#include "app_test_mode.h"

/* Private Function Declarations */

static void set_raw_digits(uint8_t d0, uint8_t d1, uint8_t d2);
static void set_u16(uint16_t value, uint8_t divisor);
static void on_i2c_rx_callback(uint8_t n_bytes);

/* Private Variables */
static DisplayData s_display_data = {
    .digits = {0,0,0},
    .dp = {false,false,false}
};

static volatile bool s_b_10_ms_tick = false;

static uint8_t s_i2c_received_data[5];
static uint8_t s_i2c_received_count;
static uint8_t s_i2c_rxtx_buf[5];
static USI_I2C_Data s_i2c_data = {
    5,
    s_i2c_rxtx_buf,
    on_i2c_rx_callback
};

/* Private Functions */

static void io_init(void)
{
    // Turn all segments off and set direction
    SEGMENT_PORT = 0x00;
    SEGMENT_DDR = 0xFF;

    // Turn all digits off and set direction
    DIGIT_PORT &= ~(DIGIT1_PINMASK | DIGIT2_PINMASK | DIGIT3_PINMASK);
    DIGIT_DDR |= (DIGIT1_PINMASK | DIGIT2_PINMASK | DIGIT3_PINMASK);

    // Pullup test pin
    TEST_PORT |= TEST_PINMASK;
}

static void app_timer_init(void)
{
    // Timer clock = 1MHz / 64 = 15.625kHz
    // Clear timer on compare mode
    TCCR1B = (1 << CTC1) | (1<<CS12) | (1<<CS11) | (1<<CS10);
    // Need to set both the 0CR1A register (which triggers the interrupt handler)
    // and the OCR1C register (which the timer CTC clear is trigger by)
    OCR1A = OCR1C = 156;     // Overflow frequency: 100Hz
    TIMSK |= (1 << OCIE1A);
}

static void set_raw_digits(uint8_t d0, uint8_t d1, uint8_t d2)
{
    s_display_data.digits[0] = d0 & 0x7F;
    s_display_data.digits[1] = d1 & 0x7F;
    s_display_data.digits[2] = d2 & 0x7F;
    s_display_data.dp[0] = d0 & 0x80;
    s_display_data.dp[1] = d1 & 0x80;
    s_display_data.dp[2] = d2 & 0x80;
}

static void set_u16(uint16_t value, uint8_t divisor)
{
    s_display_data.digits[0] = value / 100;
    s_display_data.digits[1] = (value % 100) / 10;
    s_display_data.digits[2] = value % 10;

    s_display_data.dp[0] = divisor == 100;
    s_display_data.dp[1] = divisor == 10;
    s_display_data.dp[2] = divisor == 1;
}

static void on_i2c_rx_callback(uint8_t n_bytes)
{
    memcpy(s_i2c_received_data, s_i2c_rxtx_buf, n_bytes);
    s_i2c_received_count = n_bytes;
}

int main(void)
{

    io_init();

    display_init(&s_display_data);
    app_timer_init();

    USI_I2C_Slave_Initialise(&s_i2c_data, 0x10);

    sei();

    while (true)
    {
        if (check_and_clear((bool*)&s_b_10_ms_tick))
        {
            if (TEST_PINS & TEST_PINMASK)
            {
                display_blink_tick();
            }
            else
            {
                test_mode_tick(&s_display_data);
            }
        }

        if (s_i2c_received_count)
        {
            uint16_t u16_data;
            memcpy(&u16_data, &s_i2c_received_data[1], sizeof(uint16_t));

            switch((APPI2CRegisterType)s_i2c_received_data[0])
            {
            case APPI2CRegisterType_RawDigits:
                set_raw_digits(s_i2c_received_data[1], s_i2c_received_data[2], s_i2c_received_data[3]);
                break;
            case APPI2CRegisterType_U16:
                {
                    uint16_t divisor = 0;
                    if (s_i2c_received_count == 4)
                    {
                        divisor = s_i2c_received_data[3];
                    }
                    set_u16(u16_data, divisor);
                }
                break;
            case APPI2CRegisterType_BlinkOnInterval:
                display_blink_set_on_interval(u16_data);
                break;
            case APPI2CRegisterType_BlinkOffInterval:
                display_blink_set_off_interval(u16_data);
                break;
            case APPI2CRegisterType_Enable:
                display_enable(s_i2c_received_data[1]);
                break;
            }
            s_i2c_received_count = 0;
        }
    }
}

ISR(TIMER1_CMPA_vect)
{
    // 10 ms tick
    s_b_10_ms_tick = true;
}
