// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
*
* Atmel Corporation
*
* File              : USI_I2C_Slave.c
* Compiler          : IAR EWAAVR 4.11A
* Revision          : $Revision: 614 $
* Date              : $Date: 2006-07-07 16:27:05 +0800 (Fri, 07 Jul 2006) $
* Updated by        : $Author: jllassen $
*
* Support mail      : avr@atmel.com
*
* Supported devices : All device with USI module can be used.
*                     The example is written for the ATmega169, ATtiny26, ATmega325 & ATtiny2313
*
* AppNote           : AVR312 - Using the USI module as a I2C slave
*
* Description       : Functions for USI_I2C_receiver and USI_I2C_transmitter.
*
*
****************************************************************************/
/*
 * C Includes
 */

#include <stdbool.h>

/*
 * AVR Includes
 */

 #ifdef __GNUC__
#include "avr/io.h"
#include "avr/interrupt.h"
#elif __ICCAVR__
#include "ioavr.h"
#include "inavr.h"
#endif

/*
 * Library Includes
 */

#include "util_macros.h"
#include "usi_i2c_defs.h"
#include "usi_i2c_slave.h"

//********** Static Variables **********//

static uint8_t          s_u8_this_slave_address;
static volatile uint8_t s_u8_overflow_state;
static USI_I2C_Data     *sp_twi_data;

static uint8_t s_u8_rx_idx = 0;
static uint8_t s_u8_tx_idx = 0;

/*=========================> Locals <=======================================*/

//********** USI_TWI functions **********//

/*----------------------------------------------------------
  Initialise USI for TWI Slave mode.
----------------------------------------------------------*/
void USI_I2C_Slave_Initialise(USI_I2C_Data * p_data, uint8_t u8_own_address)
{
    sp_twi_data = p_data;

    s_u8_this_slave_address = u8_own_address;

    PORT_USI_CL |= (1 << PORT_USI_SCL);     // Set SCL high
    PORT_USI |= (1 << PORT_USI_SDA);        // Set SDA high
    DDR_USI_CL |= (1 << PORT_USI_SCL);      // Set SCL as output
    DDR_USI &= ~(1 << PORT_USI_SDA);        // Set SDA as input
    USICR = (1 << USISIE) | (0 << USIOIE) | // Enable Start Condition Interrupt. Disable Overflow Interrupt.
            (1 << USIWM1) | (0 << USIWM0) | // Set USI in Two-wire mode. No USI Counter overflow prior
                                            // to first Start Condition (potentail failure)
            (1 << USICS1) | (0 << USICS0) | (0 << USICLK) | // Shift Register Clock Source = External, positive edge
            (0 << USITC);
    USISR = 0xF0; // Clear all flags and reset overflow counter
}

// Disable USI for TWI Slave mode.
void USI_I2C_Slave_Disable()
{
    DDR_USI_CL &= ~(1 << PORT_USI_SCL);  // Set SCL as input
    DDR_USI &= ~(1 << PORT_USI_SDA);  // Set SDA as input
    USICR = 0x00; // Disable USI
    USISR = 0xF0; // Clear all flags and reset overflow counter
}

// Puts data in the transmission buffer
#ifdef ENABLE_I2C_TX
void USI_I2C_Transmit_Byte(unsigned char data)
{
    sp_twi_data->pu8_tx_buf[s_u8_tx_idx++] = data;          // Store data in buffer.
}
#endif

// Check if there is an active data session.
bool USI_I2C_Slave_Is_Active()
{
    // Active if Overflow Interrupt is enabled and no Stop Condition occurred
    return (USICR & (1 << USIOIE)) && ((USISR & (1 << USIPF)) == 0);
}

/*----------------------------------------------------------
 Detects the USI_TWI Start Condition and intialises the USI
 for reception of the "TWI Address" packet.
----------------------------------------------------------*/
#ifdef __GNUC__
ISR(USI_START_VECTOR)
#elif __ICCAVR__
#pragma vector = USI_START_VECTOR
__interrupt void USI_Start_Condition_ISR(void)
#endif
{
    unsigned char tmpPin; // Temporary variable for pin state

    s_u8_overflow_state = USI_SLAVE_CHECK_ADDRESS;
    DDR_USI &= ~(1 << PORT_USI_SDA); // Set SDA as input

    // Wait for SCL to go low to ensure the "Start Condition" has completed.
    // If a Stop condition arises then leave the interrupt to prevent waiting forever.
    while ((tmpPin = (PIN_USI_CL & (1 << PORT_USI_SCL))) && ((PIN_USI & (1 << PIN_USI_SDA)) == 0)) {}

    if (tmpPin & (1 << PIN_USI_SDA))
    {
        // Stop Condition (waiting for next Start Condition)
        USICR = (1 << USISIE) | (0 << USIOIE) | // Enable Start Condition Interrupt. Disable Overflow Interrupt.
                (1 << USIWM1) | (0 << USIWM0) | // Set USI in Two-wire mode. No USI Counter overflow prior
                                                // to first Start Condition (potentail failure)
                (1 << USICS1) | (0 << USICS0) | (0 << USICLK) | // Shift Register Clock Source = External, positive edge
                (0 << USITC);
        s_u8_rx_idx = s_u8_tx_idx = 0;
    }
    else
    {
        // Call slave receive callback if there is data in the buffer (indicates a repeated start)
        if (s_u8_rx_idx && sp_twi_data->fn_rx_complete)
        {
            sp_twi_data->fn_rx_complete(s_u8_rx_idx);
        }
        s_u8_rx_idx = 0; // reset rx buffer

        // Start Condition (Enable Overflow Interrupt)
        USICR = (1 << USISIE) | (1 << USIOIE)
                | // Enable Overflow and Start Condition Interrupt. (Keep StartCondInt to detect RESTART)
                (1 << USIWM1) | (1 << USIWM0) |                 // Set USI in Two-wire mode.
                (1 << USICS1) | (0 << USICS0) | (0 << USICLK) | // Shift Register Clock Source = External, positive edge
                (0 << USITC);
    }
    USISR = (1 << USI_START_COND_INT) | (1 << USIOIF) | (1 << USIPF) | (1 << USIDC) | // Clear flags
            (0x0 << USICNT0); // Set USI to sample 8 bits i.e. count 16 external pin toggles.
}

/*----------------------------------------------------------
 Handels all the comunication. Is disabled only when waiting
 for new Start Condition.
----------------------------------------------------------*/
#ifdef __GNUC__
ISR(USI_OVERFLOW_VECTOR)
#elif __ICCAVR__
#pragma vector = USI_OVERFLOW_VECTOR
__interrupt void USI_Counter_Overflow_ISR(void)
#endif
{
    unsigned char tmpUSIDR;

    switch (s_u8_overflow_state)
    {
    // ---------- Address mode ----------
    // Check address and send ACK (and next USI_SLAVE_SEND_DATA) if OK, else reset USI.
    case USI_SLAVE_CHECK_ADDRESS:
        if ((USIDR == 0) || ((USIDR >> 1) == s_u8_this_slave_address))
        {
            if (USIDR & 0x01)
            {
                #ifdef ENABLE_I2C_TX
                if (sp_twi_data->fn_prepare_transmit)
                {
                    // Reset tx buffer and call callback
                    s_u8_tx_idx = 0;
                    sp_twi_data->fn_prepare_transmit();
                }
                #endif
                s_u8_overflow_state = USI_SLAVE_SEND_DATA;
            }
            else
            {
                s_u8_overflow_state = USI_SLAVE_REQUEST_DATA;
            }
            SET_USI_TO_SEND_ACK();
        }
        else
        {
            SET_USI_TO_TWI_START_CONDITION_MODE();
        }
        break;

    // ----- Master write data mode ------
    // Check reply and goto USI_SLAVE_SEND_DATA if OK, else reset USI.
    case USI_SLAVE_CHECK_REPLY_FROM_SEND_DATA:
        if (USIDR) // If NACK, the master does not want more data.
        {
            SET_USI_TO_TWI_START_CONDITION_MODE();
            return;
        }
    // From here we just drop straight into USI_SLAVE_SEND_DATA if the master sent an ACK

    // Copy data from buffer to USIDR and set USI to shift byte. Next USI_SLAVE_REQUEST_REPLY_FROM_SEND_DATA
    case USI_SLAVE_SEND_DATA:

        // Get data from Buffer
        #ifdef ENABLE_I2C_TX
        if (s_u8_tx_idx < sp_twi_data->u8_buffer_size)
        {
            USIDR = sp_twi_data->pu8_tx_buf[s_u8_tx_idx++];
        }
        else // If the buffer is empty then:
        #else
        USIDR = 0xFF;
        #endif

        {
            SET_USI_TO_TWI_START_CONDITION_MODE();
            return;
        }
        s_u8_overflow_state = USI_SLAVE_REQUEST_REPLY_FROM_SEND_DATA;
        SET_USI_TO_SEND_DATA();
        break;

    // Set USI to sample reply from master. Next USI_SLAVE_CHECK_REPLY_FROM_SEND_DATA
    case USI_SLAVE_REQUEST_REPLY_FROM_SEND_DATA:
        s_u8_overflow_state = USI_SLAVE_CHECK_REPLY_FROM_SEND_DATA;
        SET_USI_TO_READ_ACK();
        break;

    // ----- Master read data mode ------
    // Set USI to sample data from master. Next USI_SLAVE_GET_DATA_AND_SEND_ACK.
    case USI_SLAVE_REQUEST_DATA:
        s_u8_overflow_state = USI_SLAVE_GET_DATA_AND_SEND_ACK;
        SET_USI_TO_READ_DATA();
        // call slave receive callback on stop condition
        if (s_u8_rx_idx)
        {
            // There is data in receive buffer
            // Check for stop Condition
            while ((USISR & ((1 << USI_START_COND_INT) | (1 << USIPF) | (0xE << USICNT0))) == 0) {}
            // Wait for either Start or Stop Condition
            // Cancel after one SCL cycle
            if (USISR & (1 << USIPF))
            {
                // Stop Condition
                if (sp_twi_data->fn_rx_complete)
                {
                    sp_twi_data->fn_rx_complete(s_u8_rx_idx);
                }
                s_u8_rx_idx = 0; // reset rx buffer
            }
        }
        break;

    // Copy data from USIDR and send ACK. Next USI_SLAVE_REQUEST_DATA
    case USI_SLAVE_GET_DATA_AND_SEND_ACK:
        // Put data into Buffer
        s_u8_overflow_state = USI_SLAVE_REQUEST_DATA;
        tmpUSIDR              = USIDR; // Not necessary, but prevents warnings
        if (s_u8_rx_idx < sp_twi_data->u8_buffer_size)
        {
            sp_twi_data->pu8_rx_buf[s_u8_rx_idx++] = tmpUSIDR;
            SET_USI_TO_SEND_ACK();
        }
        else // If the buffer is full then:
        {
            SET_USI_TO_SEND_NACK();
        }
        break;
    }
}
