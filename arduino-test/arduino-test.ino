#include <Wire.h>

#define DIGIT_CMD 0x00
#define NUMERIC_CMD 0x01
#define BLINK_ON_INTERVAL_CMD 0x02
#define BLINK_OFF_INTERVAL_CMD 0x03
#define ENABLE_DISABLE_CMD 0x04

void setup() {
  randomSeed(analogRead(0));
  Wire.begin();
}

void loop() {
  // Mode 1
  {
    uint8_t data[] = {DIGIT_CMD, 0x00, 0x00, 0x00};
    for (int i=0; i<4; i++)
    {
      for (int j=1; j<4; j++)
      {
        data[j] = random(0,10);
        if (random(0,2)) { data[j] |= 0x80; }
        Wire.beginTransmission(0x10);
        Wire.write(data, 4);
        Wire.endTransmission();
        delay(400);
      }
    }
  }
  // Mode 2
  {
    uint8_t data[] = {NUMERIC_CMD, 0x00, 0x00, 10};
    uint16_t to_display = random(100, 989);
    for (int i=0; i<10; i++)
    {
      memcpy(&data[1], &to_display, 2);
      Wire.beginTransmission(0x10);
      Wire.write(data, 4);
      Wire.endTransmission();
      to_display++;
      delay(500);
    }
  }
  // Blinking
  {
    uint8_t data[] = {BLINK_ON_INTERVAL_CMD, 100, 0x00};

    Wire.beginTransmission(0x10);
    Wire.write(data, 3);
    Wire.endTransmission();
    delay(10);

    data[0] = BLINK_OFF_INTERVAL_CMD;
    Wire.beginTransmission(0x10);
    Wire.write(data, 3);
    Wire.endTransmission();

    delay(5000);

    data[1] = data[2] = 0;
    Wire.beginTransmission(0x10);
    Wire.write(data, 3);
    Wire.endTransmission();
    delay(10);
  }
  // Display Off
  {
    uint8_t data[] = {ENABLE_DISABLE_CMD, 0x00};

    Wire.beginTransmission(0x10);
    Wire.write(data, 2);
    Wire.endTransmission();
    delay(5000);
  }

  // Display On
  {
    uint8_t data[] = {ENABLE_DISABLE_CMD, 0x01};

    Wire.beginTransmission(0x10);
    Wire.write(data, 2);
    Wire.endTransmission();
    delay(1000);
  }

}
