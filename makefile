NAME = kingbright-bc56-breakout

MCU = attiny26
F_CPU = 1000000
PROG = atmelice_isp

AVRDUDE = avrdude -c $(PROG) -p $(MCU)
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump
SIZE    = avr-size --format=avr --mcu=$(MCU)
CC      = avr-gcc

DEFINES = -DENABLE_USI_RX
LDFLAGS = -Wl,--gc-sections,--relax -lm -lc
SRC_DIR = ./src
INC_DIRS = ./inc libs/generics libs/utility/check-and-clear libs/utility
BUILD_DIR = ./build

VPATH = $(SRC_DIR) $(INC_DIRS)

SOURCES = $(wildcard $(SRC_DIR)/*.c) libs/generics/seven_segment_map.c libs/utility/check-and-clear/check-and-clear.c

OBJECTS = $(addprefix $(BUILD_DIR)/, $(notdir $(SOURCES:%.c=%.o)))

TARGET_ELF = $(BUILD_DIR)/$(NAME).elf
TARGET_HEX = $(BUILD_DIR)/$(NAME).hex

CFLAGS = -Wall -Wextra -Werror -DF_CPU=$(F_CPU) $(DEFINES) -mmcu=$(MCU) -std=gnu99 -Os -ffunction-sections -fdata-sections $(addprefix -I, $(INC_DIRS))

all: $(TARGET_HEX)

flash: all
	$(AVRDUDE) -U flash:w:$(TARGET_HEX):i

clean:
	rm -f $(BUILD_DIR)/*.o $(BUILD_DIR)/*.elf $(BUILD_DIR)/*.hex

$(BUILD_DIR)/%.o:%.c
	$(CC) $(CFLAGS) -c $< -o $@

# elf file
$(TARGET_ELF): $(OBJECTS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(TARGET_ELF) $(OBJECTS)

# hex file
$(TARGET_HEX): $(TARGET_ELF)
	$(OBJCOPY) -j .text -j .data -O ihex $(TARGET_ELF) $(TARGET_HEX)
	$(SIZE) $(TARGET_ELF)
