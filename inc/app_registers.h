#ifndef _APP_REGISTERS_H_
#define _APP_REGISTERS_H_

/* Typedefs, enums, structs */

typedef enum
{
    APPI2CRegisterType_RawDigits,
    APPI2CRegisterType_U16,
    APPI2CRegisterType_BlinkOnInterval,
    APPI2CRegisterType_BlinkOffInterval,
    APPI2CRegisterType_Enable
} APPI2CRegisterType;

#endif
