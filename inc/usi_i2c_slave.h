#ifndef USI_I2C_SLAVE_H
#define USI_I2C_SLAVE_H

#include "usi_i2c_defs.h"

void          USI_I2C_Slave_Initialise(USI_I2C_Data * p_data, uint8_t u8_own_address);
void          USI_I2C_Slave_Disable();
bool          USI_I2C_Slave_Is_Active();

#define USI_SLAVE_CHECK_ADDRESS (0x00)
#define USI_SLAVE_SEND_DATA (0x01)
#define USI_SLAVE_REQUEST_REPLY_FROM_SEND_DATA (0x02)
#define USI_SLAVE_CHECK_REPLY_FROM_SEND_DATA (0x03)
#define USI_SLAVE_REQUEST_DATA (0x04)
#define USI_SLAVE_GET_DATA_AND_SEND_ACK (0x05)

#endif
