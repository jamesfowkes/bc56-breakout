// This file has been prepared for Doxygen automatic documentation generation.
/*! \file ********************************************************************
 *
 * Atmel Corporation
 *
 * File              : USI_I2C_Slave.h
 * Compiler          : IAR EWAAVR 4.11A
 * Revision          : $Revision: 6351 $
 * Date              : $Date: 2010-01-29 20:15:43 +0800 (Fri, 29 Jan 2010) $
 * Updated by        : $Author: hskinnemoen $
 *
 * Support mail      : avr@atmel.com
 *
 * Supported devices : All device with USI module can be used.
 *                     The example is written for the ATmega169, ATtiny26 & ATtiny2313
 *
 * AppNote           : AVR312 - Using the USI module as a TWI slave
 *
 * Description       : Header file for USI_TWI driver
 *
 *
 *
 ****************************************************************************/

#ifndef USI_I2C_DEFS_H
#define USI_I2C_DEFS_H

typedef struct
{
    uint8_t u8_buffer_size;
    #ifdef ENABLE_USI_RX
    uint8_t *pu8_rx_buf;
    void  (*fn_rx_complete)(uint8_t u8_count);
    #endif
    #ifdef ENABLE_USI_TX
    uint8_t *pu8_tx_buf;
    void  (*fn_prepare_transmit)(void);
    #endif
} USI_I2C_Data;

// Device dependant defines
#include <usi_io_defs.h>

#define SET_USI_TO_SEND_ACK()                                                                                          \
    {                                                                                                                  \
        USIDR = 0;                      /* Prepare ACK                         */                                      \
        DDR_USI |= (1 << PORT_USI_SDA); /* Set SDA as output                   */                                      \
        USISR = (0 << USI_START_COND_INT) | (1 << USIOIF) | (1 << USIPF) | (1 << USIDC)                                \
                |                  /* Clear all flags, except Start Cond  */                                           \
                (0x0E << USICNT0); /* set USI counter to shift 1 bit. */                                               \
    }

#define SET_USI_TO_SEND_NACK()                                                                                          \
    {                                                                                                                  \
        DDR_USI &= ~(1 << PORT_USI_SDA); /* Set SDA as intput, NACK is SDA high */                                     \
        USISR = (0 << USI_START_COND_INT) | (1 << USIOIF) | (1 << USIPF) | (1 << USIDC)                                \
                |                  /* Clear all flags, except Start Cond  */                                           \
                (0x0E << USICNT0); /* set USI counter to shift 1 bit. */                                               \
    }

#define SET_USI_TO_READ_ACK()                                                                                          \
    {                                                                                                                  \
        DDR_USI &= ~(1 << PORT_USI_SDA); /* Set SDA as intput */                                                       \
        USIDR = 0;                       /* Prepare ACK        */                                                      \
        USISR = (0 << USI_START_COND_INT) | (1 << USIOIF) | (1 << USIPF) | (1 << USIDC)                                \
                |                  /* Clear all flags, except Start Cond  */                                           \
                (0x0E << USICNT0); /* set USI counter to shift 1 bit. */                                               \
    }

#define SET_USI_TO_TWI_START_CONDITION_MODE()                                                                          \
    {                                                                                                                  \
        DDR_USI &= ~(1 << PORT_USI_SDA); /* Set SDA as intput */                                                       \
        USICR = (1 << USISIE) | (0 << USIOIE) | /* Enable Start Condition Interrupt. Disable Overflow Interrupt.*/     \
                (1 << USIWM1) | (0 << USIWM0) | /* Set USI in Two-wire mode. No USI Counter overflow hold.      */     \
                (1 << USICS1) | (0 << USICS0) | (0 << USICLK)                                                          \
                | /* Shift Register Clock Source = External, positive edge        */                                   \
                (0 << USITC);                                                                                          \
        USISR = (0 << USI_START_COND_INT) | (1 << USIOIF) | (1 << USIPF) | (1 << USIDC)                                \
                | /* Clear all flags, except Start Cond                            */                                  \
                (0x0 << USICNT0);                                                                                      \
    }

#define SET_USI_TO_SEND_DATA()                                                                                         \
    {                                                                                                                  \
        DDR_USI |= (1 << PORT_USI_SDA); /* Set SDA as output                  */                                       \
        USISR = (0 << USI_START_COND_INT) | (1 << USIOIF) | (1 << USIPF) | (1 << USIDC)                                \
                |                 /* Clear all flags, except Start Cond */                                             \
                (0x0 << USICNT0); /* set USI to shift out 8 bits        */                                             \
    }

#define SET_USI_TO_READ_DATA()                                                                                         \
    {                                                                                                                  \
        DDR_USI &= ~(1 << PORT_USI_SDA); /* Set SDA as input                   */                                      \
        USISR = (0 << USI_START_COND_INT) | (1 << USIOIF) | (1 << USIPF) | (1 << USIDC)                                \
                |                 /* Clear all flags, except Start Cond */                                             \
                (0x0 << USICNT0); /* set USI to shift out 8 bits        */                                             \
    }

#endif
