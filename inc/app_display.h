#ifndef _APP_DISPLAY_H_
#define _APP_DISPLAY_H_

typedef struct
{
    uint8_t digits[3];
    bool dp[3];
} DisplayData;

void display_init(DisplayData * pDisplay);
void display_enable(bool en);
void display_blink_tick(void);
void display_blink_set_on_interval(uint16_t interval_ms);
void display_blink_set_off_interval(uint16_t interval_ms);
#endif
